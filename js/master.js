// select element from the DOM
const output = document.getElementById("letteroutput");

// add event "click" event listner to the button.
// the function passed in will be invoked when the event happens
document.addEventListener("keypress", function(event) {
    // this code will be called when the button is clicked:

    // get the key that was pressed from the event object
    const key = event.key;
    // ADD our class to the class list of the button
    output.innerText = key;

    // call setTimeout with 0.5 second delay
    setTimeout(function() {
        // remove the character
        output.innerText = "";
    }, 500);
});