# Event object big letters

When a key on the keyboard is pressed, display it on the page.  
When another key is pressed, replace the original letter with the new letter.

To get the key that is pressed from the event, we need to use the Event object that is passed into the function.

For a extra challenge, make the key disappear from the screen after a certain amount of time.

**Note:** Styles are provided for you in master.css. Look in the file to find the approriate class names to use on your HTML elements.

### Example

[The example is here](https://eventobject-bigletters.now.sh).

## Task

Clone or download this repository onto your computer.  You will start out in the "master" branch which contains an empty project.

Try to recreate the website above.  Firstly, try to create it without any help.  If you are unsure of what to do, you can follow the steps below.  If the steps don't help, checkout out the "answer" branch from this repository.  The answer branch contains a working example.

## Steps

1. a *div* to the page for the key to be output into.
2. Select the div in JavaScript.
3. Add event listener to listen for press presses. We want to get every keypress, so the event needs to be set on the `document` object.
4. Create a callback function that is passed into the event listener.  The callback function needs to have a parameter called event.  When the callback is fired, the event parameter will give us information about the key press.
1. Inside the callback function, use the *key* property on the *event* parameter to get the key that was pressed.
1. Set the key into the innerText of the output *div*.